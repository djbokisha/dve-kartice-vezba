import React from "react";
import { useState } from "react";

import "./Home.css"


function Home(){

    const[pera,setPera]=useState('')
    const[leviArr,setLeviArr] = useState([])
    const[desniArr,setDesniArr]=useState([])
    const[treciArr,setTreciArr]=useState([])

   // console.log(pera)


    

const getValueHandler = (e) => {
setPera(e.target.value)


}


const leviInput = (e) =>{
setLeviArr(leviArr => [...leviArr,{value:pera,id:Math.random().toString()}]);

};

const desniInput = (e) =>{
setDesniArr(desniArr => [...desniArr,{value:pera,id:Math.random().toString()}]);

};

const treciInput = (e) =>{
setTreciArr(treciArr =>[...treciArr,{value:pera,id:Math.random().toString()}]);
}




const deleteHandler = (id) => {


const updatedList = leviArr.filter(item => item.id !== id);

setLeviArr(()=> updatedList)

}

const deleteHandlerTwo = (id)=>{

const updatedList2 = desniArr.filter(item => item.id !== id);

setDesniArr(()=> updatedList2)

}

const deleteHandlerThree = (id)=>{

const updatedList3 = treciArr.filter(item => item.id !== id);

setTreciArr(()=> updatedList3)
}


const moveHandler = (id) =>{

const deletedItem = leviArr.find((itm)=> itm.id===id);
console.log(deletedItem)

setDesniArr((prevVal)=>{
return [...prevVal,deletedItem];
})

const leftToRight = leviArr.filter(item => item.id !== id);

setLeviArr(()=> leftToRight)

}

const moveHandler2 = (id) =>{

const deletedItem2 = desniArr.find((itm2)=> itm2.id===id);

setTreciArr((prevVal2)=>{
return [...prevVal2,deletedItem2];
})

const rightToThree = desniArr.filter(item => item.id !== id);


setDesniArr (()=> rightToThree)

}



const moveHandler3 = (id) =>{
const deletedItem3 = desniArr.find((itm3)=> itm3.id === id);

setLeviArr((prevVal3)=>{
  return [...prevVal3,deletedItem3];

})

const rightToFirst = desniArr.filter(item => item.id !== id);

setDesniArr(()=> rightToFirst)

}

const moveHandler4 = (id) =>{
const deletedItem4 = treciArr.find((itm4)=> itm4.id === id);

setDesniArr((prevVal4)=>{
 return [...prevVal4,deletedItem4];

})

const threeToRight = treciArr.filter(item => item.id !==id);

setTreciArr(()=> threeToRight)


}


    return (
      <div className="App">
        
  
        <main>
        
        <input type="text" placeholder="Input to do" onChange={getValueHandler} />
            <button type="button" onClick={leviInput}>levo</button>
            <button type="button" onClick={desniInput}>Desno</button>
            <button type="button" onClick={treciInput}>Treci</button>

            <div className="card">
            <div className="card1">
                <p>levo</p>
                <hr/>
                <ul>
                  {leviArr.map(item => <li key={item.id}><button onClick={()=>{deleteHandler(item.id)}}>Delete</button> <div className="txt">{item.value}</div> <button onClick={()=>{moveHandler(item.id)}}>move</button> </li>)}

                </ul>
                
              

              
            </div>
  
            <div className="card2">
                <p>desno</p>
                <hr></hr>
              <ul>
              {desniArr.map(item => <li key={item.id}><button onClick={()=>{moveHandler3(item.id)}}>move</button> {item.value}<button onClick={()=>{moveHandler2(item.id)}}>move</button></li>)}

              </ul>
            </div>

            <div className="card3">
                <p>Treca</p>
              <ul>
              {treciArr.map(item => <li key={item.id}><button onClick={()=>{moveHandler4(item.id)}}>move</button> {item.value} <button onClick={()=>{deleteHandlerThree(item.id)}}>Delete</button></li>)}

              </ul>
            </div>
          </div>
        </main>
      </div>

    );


};

export default Home;